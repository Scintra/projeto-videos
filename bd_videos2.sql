-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Mar-2020 às 18:17
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_videos2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria`
--

CREATE TABLE `tb_categoria` (
  `cod_categoria` int(11) NOT NULL,
  `nome_categoria` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `imagem_categoria` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `data_categoria` date NOT NULL,
  `hora_categoria` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_categoria`
--

INSERT INTO `tb_categoria` (`cod_categoria`, `nome_categoria`, `imagem_categoria`, `data_categoria`, `hora_categoria`) VALUES
(2, 'Noticias ', 'upload-categoria/c2.png', '2020-03-05', '15:18:34'),
(3, 'Stand Up', 'upload-categoria/humor.png', '2020-03-05', '15:18:41'),
(4, 'Filme', 'upload-categoria/filme.png', '2020-03-05', '15:18:51'),
(6, 'Música', 'upload-categoria/musica.jpg', '2020-03-05', '15:29:41'),
(7, 'Diy', 'upload-categoria/diy.jpg', '2020-03-09', '15:53:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `cod_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `senha_usuario` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `foto_usuario` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `data_usuario` date NOT NULL,
  `hora_usuario` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`cod_usuario`, `nome_usuario`, `email_usuario`, `senha_usuario`, `foto_usuario`, `data_usuario`, `hora_usuario`) VALUES
(1, 'Stela', 'stela_cintra@yahoo.com.br', '$2y$10$moBM45VH.uWrKZqH7r3wc.5rLwufCmwGjrOm4JHVLDDMbL5pte/KW', 'upload/1.png', '2020-03-10', '17:24:15'),
(2, 'maria', 'maria@gmail.com', '$2y$10$u8vW0qZgyskdVIOZ5BCtjOePUEWldn8XNlyUoyl5QyYCeoi/uHiSG', 'upload/avatar.png', '2020-03-11', '13:54:50'),
(6, 'rosa', 'rosa@gmail.com', '$2y$10$i9BYmeakPyamHt1u1X0h1OPkKbdI4WOgrvhMlAXarDlLk2o48gJT6', 'upload/avatar.png', '2020-03-12', '13:57:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_videos`
--

CREATE TABLE `tb_videos` (
  `cod_videos` int(11) NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `nome_videos` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `id_videos` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `data_videos` date NOT NULL,
  `hora_videos` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_videos`
--

INSERT INTO `tb_videos` (`cod_videos`, `cod_categoria`, `nome_videos`, `id_videos`, `data_videos`, `hora_videos`) VALUES
(4, 7, 'Permanent Magnet Free Energy Generator', 'NuPdWgVO9zQ', '2020-03-09', '14:26:12'),
(5, 2, 'Rompendo diamante', 'ptEMf1I77g0', '2020-03-09', '14:31:06'),
(7, 2, 'Coronavírus: saiba das últimas notícias no Brasil e no mundo', 'p7wituyBTWA', '2020-03-09', '15:54:37'),
(8, 2, 'Coronavírus: 10 boas notícias em meio à \"epidemia de medo\"', 'G1LoovRQQS4', '2020-03-09', '15:55:25'),
(9, 7, 'Procurando diamante na estrada !!', 'A6sZaoaP44E', '2020-03-10', '14:10:42'),
(10, 3, 'O PROFESSOR DE INGLÊS E AS ROSAS COLOMBIANAS - André Santi - Stand Up Comedy', 'M62PjRGVCps', '2020-03-10', '14:12:01'),
(11, 3, 'AFONSO PADILHA - O BRASIL TEM ESTRUTURA PRA RECEBER O CORONA?', '0OJaHGfLP1A', '2020-03-10', '14:12:23'),
(12, 3, ' AFONSO PADILHA - JEITINHO BRASILEIRO', '4d6Q7xs7G18', '2020-03-10', '14:12:58'),
(13, 4, 'O BICHO VAI PEGAR 3 FILME COMPLETO DUBLADO HD', 'Jb1Nxs3_pjE', '2020-03-10', '14:13:50'),
(14, 6, 'Melim - Meu Abrigo (Clipe Oficial)', 'gUpGTRR4Tt4', '2020-03-10', '14:14:45');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Índices para tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`cod_usuario`);

--
-- Índices para tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD PRIMARY KEY (`cod_videos`),
  ADD KEY `cod_categoria` (`cod_categoria`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  MODIFY `cod_videos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD CONSTRAINT `tb_videos_ibfk_1` FOREIGN KEY (`cod_categoria`) REFERENCES `tb_categoria` (`cod_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
