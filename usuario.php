<?php 
session_start();
include("verifica-logado.php");

?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Cadastro de Novo Usuário</title>
    <?php include("links-css-js.php"); ?>
</head>

<body>

    <div class="container-fluid">
    <?php include("menu.php"); 

    if(isset($_SESSION['imagem_ok'])){
        print" 
        <div class='alert alert-primary role='alert'>". $_SESSION['imagem_ok']."
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>&times</span></button>  
        </div> ";
        unset($_SESSION['imagem_ok']);
    }?>



    <h1 class="mt-5 mb-5"> Página do Usuário <?php print $_SESSION['nome_usuario'] ;?> </h1>

    <div class="row mt-5">

    <div class="col-sm-4">
    <?php
    print "
    <img src='".$_SESSION['foto_usuario']."' alt='".$_SESSION['nome_usuario']."' 
    title='".$_SESSION['nome_usuario']."' width='50%' class='img-thumbnail' />
    ";
    ?>
    <!-- sm comando para separar -->
    </div>

    <div class="col-sm-8">
        <h3 class="mt-3">Nome:</h3>
        <?php print $_SESSION['nome_usuario'] ;?>
        <hr/>
        <hr/>
        <h3>E-mail:</h3>
        <?php print $_SESSION['email_usuario'] ;?>
        <hr/>
        <hr/>
        <h3>Status:</h3>
        Usuário ativo no sistema


    </div>


        <!-- <div class="col-sm-3" style="border: 1px solid #ccc;">
        banana
        </div>
        <div class="col-sm-5" style="border: 1px solid #ccc;">
        morango
        </div>
        <div class="col-sm-2"style="border: 1px solid #ccc;"  >
        melancia
        </div>
        <div class="col-sm-2" style="border: 1px solid #ccc;" >
        Maçã
        </div> -->

    
    

    </div>
    <!--container end.//-->

    <div class="row mt-5 mb-5">
    <div class="col-sm">
    <form name="frm_imagem" id="frm_imagem" action="upload.php" method="post" 
    enctype="multipart/form-data">
        <h2> Modificar Imagem do Usuário</h2>
    <div class="custom-file">
    <input type="file" class="custom-file-input" id="imagem" name="imagem"/>
    <label class="custom-file-label" for="imagem"> Escolha uma imagem</label>
    </div>
    <div class="mt-3">
    <input type="submit" name="enviar" id="enviar" value="Enviar" class="btn btn-primary w-100"/>
    </div>
    </form>
    </div>

    <div class="col-sm">
    <h2> Modificar os dados do Usuario</h2>

    </div>

    </div>
<script>
        $(".custom-file-input").on("change", function() {
            var filename = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(filename);
        });
    </script>

</script>
</body>

</html>