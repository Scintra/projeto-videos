<?php

session_start();
include("verifica-logado.php");
include("conecta.php");

$sql = "SELECT cod_categoria, nome_categoria FROM tb_categoria" ;
$res = mysqli_query($_con, $sql) or die("Não foi possível realizar a consulta");

?>
<!DOCTYPE html>
<html>

<head>
    <title>Cadastro de Videos </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script>
      function mostrarResultado(str) {
         if (str.length == 0) {
            document.getElementById("divbuscar").innerHTML = "";
            document.getElementById("divbuscar").style.border = "0px";
            return;
         }
         if (window.XMLHttpRequest) {

            xmlhttp = new XMLHttpRequest();
            // ie7+, firefox, Chrome, Opera e safari
         } else {

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            // ie6 e ie5
         }

         xmlhttp.onreadystatechange = function() {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
               document.getElementById("divbuscar").innerHTML = xmlhttp.responseText;
               document.getElementById("divbuscar").style.border = "1px solid #a5acb2";

            }
         }
         xmlhttp.open("GET", "buscar-video.php?q=" + str, true);
         xmlhttp.send();
      }
   </script>

</head>

<body>

<?php
if(isset ($_SESSION['OK'])) {
print "<script> alert('".$_SESSION['OK']."');</script>";
unset($_SESSION['ok']);
}
?>
<?php include("menu.php"); ?>
   <h1>Cadastro de Videos </h1>
   <fieldset>
      <legend> Efetue o cadastro do video</legend>
      <form id="form1" name="form1" method="post" action="valida-video.php">
      Nome do Video<br>
      <input name="nome_video" required placeholder= "Entre com o nome do video" id="nome_video" type="text"/>
      Id do Video
      <input name="id_video" required placeholder= "Entre com o id do video (youtube) " id="id" type="text"/>
      <p>
        Selecione a categoria : <br>
      <select id="categoria" name="categoria">
          <?php

          while ($linha = mysqli_fetch_array($res)) {
            print " <option value='$linha[0]'>$linha[1] </option>";
         
          }
     
          ?>

          
    </select>
    
          </p>
          <P>
          <input name="enviar" id="enviar" value="Enviar" type="submit" />
        </p>
         </form>
 </fieldset>

 <h2>Escreva sua consulta...</h2>
   <form name="frm_consulta" id="frm_consulta">
      <input onkeyup="mostrarResultado(this.value)" name="busca" id="busca" type="text" placeholder="Digite um nome..." />
   </form>

   <h3>Categorias Cadastradas</h3>

   <?php
  include("conecta.php");
   $sql = "SELECT nome_videos, cod_videos , id_videos FROM tb_videos";
   $res = mysqli_query($_con, $sql);

   print "<div id='divbuscar'>";

   print "
<table width='100%' border='0'>
<tr>
<th class='tabela'>Nome Video/Imagem </th>
<th class='tabela'>Editar</th>
<th class='tabela'>Apagar</th>
</tr>
";
   while ($linha = mysqli_fetch_array($res)) {
      print "
      <tr class='marca_linha'>
         <td class='tabela'><a href='video.php?cod=$linha[1]'><img src='https://i.ytimg.com/vi/$linha[2]/hqdefault.jpg' alt='$linha[0]' width='300'/></a></td>
         <td class='tabela'><a href='editar-video.php?cod=$linha[1]'>Editar</a></td>
         <td class='tabela'><a href='apagar-video.php?cod=$linha[1]'>Apagar</a></td>
      </tr>
   ";
   }

   print "</table></div>";

   mysqli_close($_con); 

   ?>

</body>

</html>