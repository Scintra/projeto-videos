<?php
session_start();
include("verifica-logado.php");
include("conecta.php");
include("funcao.php");
include('links-css-js.php');

$codigo = LimpaString($_GET['cod']);

$sql = "SELECT cod_categoria, nome_categoria , imagem_categoria FROM tb_categoria WHERE cod_categoria = $codigo";
$res = mysqli_query($_con, $sql);
$linha = mysqli_fetch_array($res);
mysqli_close($_con);


?>

<!DOCTYPE html>
<html>

<head>
   <title>Editar Categoria</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width inicial-scale=1.0">
   <link href="style.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?php include("menu.php"); ?>
   
   <fieldset>
      <legend>Efetue a edição da categoria:</legend>
      <form id="form1" name="form1" method="post" action="valida-editar-categoria.php">
         <p>Nome:<br />
            <input name="nome_categoria" required id="nome_categoria" type="text" value="<?php print $linha[1]; ?>" />
         </p>
         <input name="cod" id="cod" type="hidden" value="<?php print $linha[0]; ?>" />
         <p>
            <input name="enviar" id="enviar" value="Enviar" type="submit" />
         </p>
      </form>
   </fieldset>

      <h3> Foto da categoria :</h3>
         
      <?php print "<img src='$linha[2]' />"; ?> 
         

   <h2> Modificar Imagem da categoria </h2>
   <form name="frm_imagem" id="frm_imagem" action="upload-categoria.php" method="post" enctype="multipart/form-data">

    <div class="custom-file">
    <input type="file" class="custom-file-input" id="imagem" name="imagem"/>
    <label class="custom-file-label" for="imagem"> Escolha uma imagem </label>
    </div>
    <div class="mt-3">
    <input type="submit" name="enviar" id="enviar" value="Enviar" class="btn btn-primary w-100"/>
    </div>
    <input name="cod1" id="cod1" type="hidden" value="<?php print $linha[0]; ?>" />
    </form>
    </div>

    <script>
        $(".custom-file-input").on("change", function() {
            var filename = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(filename);
        });
    </script>
</body>

</html>