<?php
session_start();

include("funcao.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>VÍdeos Selecionados para Você</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <?php include("menu.php"); ?>
    <div class="principal">
    
        <h1>
        <?php
        if(isset($_SESSION['cod_usuario'])) {
        print "Olá , " . $_SESSION['nome_usuario'] . ".";
        }
        ?>
        Meu primeiro design Responsivo</h1>
        <form name="frm_categoria" id="frm_categoria" method="post" action="index.php">
            <p>
                Selecione a categoria:<br>
                <select id="categoria" name="categoria">
                    <?php
                    include("conecta.php");
                    $sql1 = "SELECT cod_categoria, nome_categoria FROM tb_categoria";
                    $res1 = mysqli_query($_con, $sql1) or die("Não foi possível realizar a consulta.");

                    while ($linha1 = mysqli_fetch_array($res1)) {
                        print "<option value='$linha1[0]'>$linha1[1]</option>";
                    }

                    ?>
                </select>
                <input name="enviar" id="enviar" value="Enviar" type="submit" />

            </p>
        </form>

        <?php

        if (empty($_POST['categoria'])) {

            $sql = "SELECT cod_videos, nome_videos, id_videos FROM tb_videos";
        } else {
            $categoria = LimpaString($_POST['categoria']);

            $sql = "SELECT cod_videos, nome_videos, id_videos FROM tb_videos WHERE cod_categoria = $categoria";
        }

        $res = mysqli_query($_con, $sql) or die();
        $i = 0;
        print "<div class='flex-container'>";
        while ($linha = mysqli_fetch_array($res)) {
            print "<div>
            <a href='video.php?cod=$linha[0]'>    
            <img src='https://i.ytimg.com/vi/$linha[2]/hqdefault.jpg' width='100%' alt='$linha[1]' title='$linha[1]' />
                <p class='texto-video'>$linha[1]</p> </a>
                </div>                
                ";
            $i = $i + 1;
            if ($i == 3) {
                print "</div><div class='flex-container'>";
                $i = 0;
            }
        }

        if ($i == 0) {
            print "</div>";
        }
        if ($i == 1) {
            print "<div></div>
            <div></div>
            </div>
            
            ";
        }
        if ($i == 2) {
            print "<div></div>
            </div>
            
            ";
        }
        if ($i == 3) {
            print "
            </div>
            
            ";
        }

        ?>

    </div>
</body>

</html>