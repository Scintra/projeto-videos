<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title> Faça seu login no site </title>
    <?php include("links-css-js.php"); ?>
</head>

<body>

    <div class="container">
    <?php include("menu.php"); ?>
    <br>

        <div class="card bg-light">
            <article class="card-body mx-auto">
                <h4 class="card-title mt-3 text-center"> Login</h4>


                <form name="frm-usuario" id="frm-usuario" method="post" action="valida-login.php">
    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" id="email" class="form-control" placeholder="Entre com o seu e-mail" type="email">
                    </div>


                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="senha" id="senha" class="form-control" placeholder="Digite sua senha" type="password">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Entrar </button>
                    </div> <!-- form-group// -->
                    
                </form>
            </article>
        </div> <!-- card.// -->

    </div>
    <!--container end.//-->

</body>

</html>